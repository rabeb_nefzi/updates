package config;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.reporters.Files;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import executionEngine.DriverScriptTest;
import utility.Log;

import static executionEngine.DriverScriptTest.OR;


public class ActionKeywords {
			
		public static WebDriver driver;
		public static ExtentReports reports;
		public static ExtentTest logger;
		
		public ActionKeywords(){
			reports=new ExtentReports("src/test/java/Reports/Report.html");
		}

		public static void openBrowser(String object, String data){
			try{
				Log.info("Opening Browser");
				//If value of the parameter is Chrome, this will execute
				if (data.equals("Chrome")){
					System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/"+"chromedrivers\\chromedriver.exe");
					driver=new ChromeDriver();	
					logger.log(LogStatus.PASS, "Succefully opened Browser -"+ data);
					Log.info("Chrome browser started");}
				else if (data.equals("IE")){
					//You may need to change the code here to start IE Driver
					driver=new InternetExplorerDriver();
					logger.log(LogStatus.PASS, "Succefully opened Browser -"+ data);
					Log.info("IE browser started");}
				else if(data.equals("Mozilla")){
					driver=new FirefoxDriver();
					logger.log(LogStatus.PASS, "Succefully opened Browser -"+ data);
					Log.info("Mozilla browser started");}
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
				//This block will execute only in case of an exception
			}catch(Exception e){
				//This is to print the logs - Method Name & Error description/stack
				Log.info("Not able to open Browser --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to opened Browser -"+ data);
				//Set the value of result variable to false
				DriverScriptTest.bResult = false;
			}
			
		}
		
		public static void navigate(String object, String data){
			try{
				Log.info("Navigating to URL "+ "'" + Constants.URL+"'");
				driver.manage().window().maximize();
				driver.get(Constants.URL);
				logger.log(LogStatus.PASS, "Succefully Navigated to URL - "+ Constants.URL);
			}catch(Exception e){
				Log.info("Not able to navigate --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Navigate to URL - "+ Constants.URL);
				DriverScriptTest.bResult = false;
			}
				
		}
		
		public static void input_valid_username(String object, String data){
			try{
				Log.info("Entering the username in  "+ object);
				//Constant Variable is used in place of UserName
				//This is fetching the name of the element from the Object Repository property file
				driver.findElement(By.name(OR.getProperty(object))).sendKeys(data);
				logger.log(LogStatus.PASS, "Succefully entered the "+ object);
				
			}catch(Exception e){
				Log.error("Not able to Enter UserName --- " + e.getMessage());
				DriverScriptTest.bResult = false;
				logger.log(LogStatus.FAIL, "Not able to enter "+ object);
			}
						 
		}

		public static void input_valid_password (String object, String data){
			try{
				Log.info("Entering the password in "+ object);
				//Constant Variable is used in place of UserName
				//This is fetching the name of the element from the Object Repository property file
				driver.findElement(By.name(OR.getProperty(object))).sendKeys(data);
				logger.log(LogStatus.PASS, "Succefully entered the "+ object);
			}catch(Exception e){
				Log.error("Not able to Enter Password --- " + e.getMessage());
				DriverScriptTest.bResult = false;
				logger.log(LogStatus.FAIL, "Not able to enter "+ object);
			}
	 
		}
		
		
		public static void input_invalid_username(String object, String data){
			try{
				Log.info("Entering the username in "+ object);
				//Constant Variable is used in place of UserName
				//This is fetching the name of the element from the Object Repository property file
				driver.findElement(By.name(OR.getProperty(object))).sendKeys(data);
				logger.log(LogStatus.FAIL, "Invalid "+ object);
			}catch(Exception e){
				Log.error("Not able to Enter UserName --- " + e.getMessage());
				DriverScriptTest.bResult = false;
				logger.log(LogStatus.FAIL, "Not able to enter "+ object);
			}
			 
		}
		
		public static void input_invalid_password (String object, String data){
			try{
				Log.info("Entering the password in "+ object);
				//Constant Variable is used in place of UserName
				//This is fetching the name of the element from the Object Repository property file
				driver.findElement(By.name(OR.getProperty(object))).sendKeys(data);
				logger.log(LogStatus.FAIL, "Invalid "+ object);
			}catch(Exception e){
				Log.error("Not able to Enter UserName --- " + e.getMessage());
				DriverScriptTest.bResult = false;
				logger.log(LogStatus.FAIL, "Not able to enter "+ object);
			}
			 
		}
				
		public static void click(String object, String data){
			try{
		
				Log.info("login to the platform "+ object);
				driver.findElement(By.className(OR.getProperty(object))).click();
				logger.log(LogStatus.FAIL, "Unable to login to the platform");

			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
	 			DriverScriptTest.bResult = false;
			}
			
		}
		
		public static void login(String object, String data){
			try{
		
				Log.info("login to the platform "+ object);
				driver.findElement(By.className(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully login to the platform");

			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
	 			DriverScriptTest.bResult = false;
			}
			
		}
		
		public static void waitFor(String object, String data) throws Exception{
			try{
				Log.info("Wait for 5 seconds");
				Thread.sleep(2000);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}
			
		}
		
		public static void yaisServices(String object, String data) throws Exception{
			try{
				Log.info("Succefully Clicking on Webelement "+ object);
				driver.findElement(By.linkText(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void qualityGates(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.findElement(By.linkText(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		public static void spellCheck(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.findElement(By.name(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void suggestion(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.findElement(By.name(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void languageList(String object, String data) throws Exception{
			try{
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
				Log.info("Clicking on Webelement "+ object);
				driver.findElement(By.cssSelector(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void languageSelect(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
				driver.findElement(By.cssSelector(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully selecting the language");
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void uploidFile(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
				driver.findElement(By.xpath(OR.getProperty(object))).sendKeys(System.getProperty("user.dir")+"/"+"Test_Files\\"+data);  
				logger.log(LogStatus.PASS, "Succefully uploid the file"+data);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		
		public static void uploidTRS_SonixFile(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
				driver.findElement(By.cssSelector(OR.getProperty(object))).sendKeys(System.getProperty("user.dir")+"/"+"Test_Files\\"+data);  
				logger.log(LogStatus.PASS, "Succefully uploid the file"+data);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void converter(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.findElement(By.cssSelector(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}

		public static void convert(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
				driver.findElement(By.id(OR.getProperty(object))).click();
				//String extension=getExtensionByApacheCommonLib(data);
				//System.out.println(extension);
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void downloadSONIXfile(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(7000));
				driver.findElement(By.cssSelector(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void JsonValidator(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.findElement(By.cssSelector(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void VerifyJsonFile(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15)); 
				driver.findElement(By.cssSelector(OR.getProperty(object))).click(); 
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(500));
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void VerifyIncorrectJsonFile(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15)); 
				driver.findElement(By.cssSelector(OR.getProperty(object))).click(); 
		 		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));   
		 		//driver.findElement(By.linkText("['value', 'segments', 0]")).click(); 
		 		//WebElement FirstElement = driver.findElement(By.id("idvalue")); 
		 		//detect nb of issues
		 		WebElement nb_issues = driver.findElement(By.cssSelector(".card-body > .card:nth-child(1) > .card-header"));
		 		String nb=nb_issues.getText();
		 		System.out.println(nb_issues.getText());		  
		 		String numberOnly= nb.replaceAll("[^0-9]", "");
		 		System.out.println(numberOnly);
		 		int number = Integer.parseInt(numberOnly);
		 		for(int i = 1; i <= number; i++){
		 			WebElement issues = driver.findElement(RelativeLocator.with(By.tagName("a")).below(By.cssSelector(".card:nth-child("+i+") > .card-header")));
		 			issues.click();
		 			}
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void YaiEditor(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.findElement(By.linkText(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void pick_annotation(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				WebElement transcription = driver.findElement(RelativeLocator.with(By.tagName("a")).below(By.cssSelector(OR.getProperty(object))));
				transcription.click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void UploidFile(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
				driver.findElement(By.id(OR.getProperty(object))).sendKeys(System.getProperty("user.dir")+"/"+"Test_Files\\"+data);
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void ExportFile(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				 driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
				 driver.findElement(By.cssSelector(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void ClickExtensionList(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.findElement(By.id(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void SelectExtension(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				 Select drpCountry = new Select (driver.findElement (By.id("fileExtension")));
				 drpCountry.selectByVisibleText("JSON file (*.json)");       
				 driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}	
		
		public static void downloadfile(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(7000));
				driver.findElement(By.cssSelector(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void Segmenter(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
			 	driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10)); 
			 	//driver.findElement(By.cssSelector(OR.getProperty(object))).click(); 
			 	List<WebElement> totaltextarea = driver.findElements(By.tagName("textarea"));
				 int textarea=totaltextarea.size()/2;
				 System.out.println("nb of text areas before refresh is  " + totaltextarea.size());
				 System.out.println("half nb of text areas before refresh is " + textarea);

				 if (textarea>0)
				 {
				 for(int i = 1; i <= textarea-1; i++){
					 	driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20)); 
					 	driver.findElement(By.cssSelector(OR.getProperty(object))).click();
					 	driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
					 	driver.findElement(By.className("MuiSvgIcon-root")).click(); 
					 	WebElement query = driver.findElement(By.id("segment0"));
					 	query.sendKeys(Keys.CONTROL + "a");
					 	query.sendKeys(Keys.DELETE);
					 	//driver.findElement(By.id("segment0")).clear();
			 	 	//WebElement text_boxes = driver.findElement(By.id("segment"+i));
				     // enter text
			 		//text_boxes.sendKeys("Selenium");
			 	//	System.out.println("The number of textboxes is before" + textarea);
				    //driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));  	 		
				   // driver.findElement(By.id("div:nth-child(2) > .yaiSegmentContainer:nth-child(1) .MuiInputAdornment-root:nth-child(4) .MuiSvgIcon-root:nth-child(1)")).click();
				    System.out.println("Click1-----------------------------Click1");
				    System.out.println("op nb" + i);
					 driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10)); 
					 driver.findElement(By.cssSelector(".yaiTasksScreen > div:nth-child(1) > .btn")).click();
				    System.out.println("Click2-----------------------------Click2");
					logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
				 }

			     }
				 else 
				 {
					 driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10)); 
					 driver.findElement(By.cssSelector(".yaiTasksScreen > div:nth-child(1) > .btn")).click();	 
				 }

			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void Drafter(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
				driver.findElement(By.cssSelector(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void QualityCheck(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				 driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));	 
				 driver.findElement(By.id(OR.getProperty(object))).click();
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void ScrollPage(String object, String data) throws Exception{
			try{
				Log.info("Clicking on Webelement "+ object);
				 WebElement last_textarea = driver.findElement(By.id(OR.getProperty(object)));            
		         JavascriptExecutor jse = (JavascriptExecutor)driver;
		         for (int second = 0;; second++) {
		             if(second >=100000){
		                 break;
		             }
		             jse.executeScript("arguments[0].scrollTop = arguments[0].scrollTop + arguments[0].offsetHeight;",last_textarea);
		             Thread.sleep(3000);
		         }
				logger.log(LogStatus.PASS, "Succefully Clicking on the "+object);
			}catch(Exception e){
				Log.error("Not able to click --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to Click on "+ object);
				DriverScriptTest.bResult = false;
			}			
		}
		
		public static void closeBrowser(String object, String data){
			try{
				Log.info("Succefully Closing the Browser");
				driver.quit();
				logger.log(LogStatus.PASS, "Succefully closed the browser -"+ object);
			}catch(Exception e){
				Log.error("Not able to Close the Browser --- " + e.getMessage());
				logger.log(LogStatus.FAIL, "Unable to close the browser -"+ object);
				DriverScriptTest.bResult = false;
			}
			
		}
		

		
}